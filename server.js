/* eslint-disable */
// This file is used only in development mode
// To proxy all requests to backend and enable https
const { createServer } = require("https");
const { parse } = require("url");
const next = require("next");
const fs = require("fs");

require("dotenv").config({
  path: "./env.local",
});

const dev = process.env.NEXT_PUBLIC_NODE_ENV !== "prod";
const app = next({ dev });
const handle = app.getRequestHandler();

const httpsOptions = {
  key: fs.readFileSync("./certs/localhost.key"),
  cert: fs.readFileSync("./certs/localhost.crt"),
};

const PORT = process.env.NEXT_PUBLIC_PORT || 4444;

app.prepare().then(() => {
  createServer(httpsOptions, (req, res) => {
    const parsedUrl = parse(req.url, true);
    handle(req, res, parsedUrl);
  }).listen(PORT, (err) => {
    if (err) throw err;
    console.log(`> Server started on https://localhost:${PORT}`);
  });
});
