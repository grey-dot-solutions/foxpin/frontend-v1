import { QueryClient, QueryClientProvider } from 'react-query';
import {
  MuiThemeProvider, makeStyles, Theme, createStyles,
} from '@material-ui/core';
import PropTypes from 'prop-types';

// Importing MUI Theme
import MuiTheme from '../../utils/material-ui';

// AUTH HOC
import Auth from '../Auth';

// Material UI
const useStyles = makeStyles((theme: Theme) => createStyles({
}));

const queryClient = new QueryClient();

const Layout = ({ children }) => {
  const classes = useStyles();

  return (
    <MuiThemeProvider theme={MuiTheme}>
      <QueryClientProvider client={queryClient}>
        <Auth>
          {children}
        </Auth>
      </QueryClientProvider>
    </MuiThemeProvider>
  );
};

// DEFINING PROP TYPES AND DEFAULT PROPS
Layout.propTypes = {
  children: PropTypes.node,
};

Layout.defaultProps = {
  children: null,
};

export default Layout;
