import axios from '../axios';

export const emailLogin = async (req: any) => {
  const user = await axios.post('/auth/signin', {
    email: req.email,
    password: req.password,
  });

  return user.data;
};

export const userLogout = async () => {
  await axios.delete('/logout');
};
