import { createContext } from 'react';

import { IUser } from '../types';

const AuthContext = createContext<IUser>({
  isAuthenticated: false,
  fullName: '',
  firstName: '',
  middleName: '',
  lastName: '',
  role: '',
});

export default AuthContext;
