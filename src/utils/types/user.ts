export interface IUser {
    isAuthenticated: boolean;
    fullName: string;
    firstName: string;
    middleName: string;
    lastName: string;
    role: string;
}
